{application,cache_tab,
             [{description,"In-memory cache Erlang / Elixir library"},
              {vsn,"1.0.2"},
              {modules,[cache_tab,cache_tab_app,cache_tab_sup]},
              {registered,[]},
              {applications,[kernel,stdlib]},
              {mod,{cache_tab_app,[]}},
              {files,["src/","rebar.config","rebar.config.script","README.md",
                      "LICENSE.txt"]},
              {licenses,["Apache 2.0"]},
              {maintainers,["ProcessOne"]},
              {links,[{"Github","https://github.com/processone/cache_tab"}]}]}.
