#!/bin/bash
IPT=iptables
# Max connection in seconds
SECONDS=100
# Max connections per IP
BLOCKCOUNT=10

# default action can be DROP or REJECT
DACTION="DROP"
$IPT -A INPUT -p tcp --dport 80 -i eth0 -m state --state NEW -m recent --set
$IPT -A INPUT -p tcp --dport 80 -i eth0 -m state --state NEW -m recent --update --seconds ${SECONDS} --hitcount ${BLOCKCOUNT} -j ${DACTION}

$IPT -A INPUT -p tcp --dport 443 -i eth0 -m state --state NEW -m recent --set
$IPT -A INPUT -p tcp --dport 443 -i eth0 -m state --state NEW -m recent --update --seconds ${SECONDS} --hitcount ${BLOCKCOUNT} -j ${DACTION}

$IPT -A INPUT -p tcp --dport 5222 -i eth0 -m state --state NEW -m recent --set
$IPT -A INPUT -p tcp --dport 5222 -i eth0 -m state --state NEW -m recent --update --seconds ${SECONDS} --hitcount ${BLOCKCOUNT} -j ${DACTION}

$IPT -A INPUT -p tcp --dport 5280 -i eth0 -m state --state NEW -m recent --set
$IPT -A INPUT -p tcp --dport 5280 -i eth0 -m state --state NEW -m recent --update --seconds ${SECONDS} --hitcount ${BLOCKCOUNT} -j ${DACTION}

$IPT -A INPUT -p tcp --dport 5269 -i eth0 -m state --state NEW -m recent --set
$IPT -A INPUT -p tcp --dport 5269 -i eth0 -m state --state NEW -m recent --update --seconds ${SECONDS} --hitcount ${BLOCKCOUNT} -j ${DACTION}


